#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TABLE_SIZE 128

struct node {
  char* key;
  int value;
  struct node* next;
};

struct hash_table {
  struct node** table;
};

// calculo da posição na tabela 
int hash_function(char* key) {
  int sum = 0;
  int i;
  for (i = 0; i < strlen(key); i++) {
    sum += key[i];
  }
  return sum % TABLE_SIZE;
}

// Inicialização da tabela 
struct hash_table* create_hash_table() {
  int i;
  struct hash_table* ht = (struct hash_table*)malloc(sizeof(struct hash_table));
  ht->table = (struct node**)malloc(TABLE_SIZE * sizeof(struct node*));
  for (i = 0; i < TABLE_SIZE; i++) {
    ht->table[i] = NULL;
  }
  return ht;
}

// Busca 
struct node* search(struct hash_table* ht, char* key) {
  int pos = hash_function(key);
  struct node* entry = ht->table[pos];
  while (entry != NULL) {
    if (strcmp(entry->key, key) == 0) {
      return entry;
    }
    entry = entry->next;
  }
  return NULL;
}

// Inserção 
void insert(struct hash_table* ht, char* key, int value) {
  int pos = hash_function(key);
  struct node* entry = ht->table[pos];
  while (entry != NULL && strcmp(entry->key, key) != 0) {
    entry = entry->next;
  }
  if (entry == NULL) {
    struct node* new_node = (struct node*)malloc(sizeof(struct node));
    new_node->key = (char*)malloc(strlen(key) + 1);
    strcpy(new_node->key, key);
    new_node->value = value;
    new_node->next = ht->table[pos];
    ht->table[pos] = new_node;
  } else {
    entry->value = value;
  }
}

// Deleção
void delete(struct hash_table* ht, char* key) {
  int pos = hash_function(key);
  struct node* entry = ht->table[pos];
  struct node* prev = NULL;
  while (entry != NULL && strcmp(entry->key, key) != 0) {
    prev = entry;
    entry = entry->next;
  }
  if (entry == NULL) {
    return;
  }
  if (prev == NULL) {
    ht->table[pos];
  }
